module "environment_tls" {
  source = "./modules/tls-certificate"

  certificate_name     = "${module.route53_root_zone.domain_name}-san"
  domain_name          = module.route53_root_zone.domain_name
  dns_zone_domain_name = "${module.route53_root_zone.domain_name}"

  subject_alternative_names = ["www.${module.route53_root_zone.domain_name}"]

  providers = {
    aws = aws.useast
  }

  tags = merge(
    local.tags,
    {
      Name = "Environment TLS certificate"
    },
  )
}
