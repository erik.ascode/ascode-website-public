locals {
  create_dns_validation_records = var.process_domain_validation_options && var.validation_method == "DNS"

  cert_name_count = 1 + length(var.subject_alternative_names)
}

resource "aws_route53_record" "validation" {
  count = local.create_dns_validation_records ? local.cert_name_count : 0

  ttl = var.ttl

  name = aws_acm_certificate.default.domain_validation_options[count.index]["resource_record_name"]
  type = aws_acm_certificate.default.domain_validation_options[count.index]["resource_record_type"]

  records = [aws_acm_certificate.default.domain_validation_options[count.index]["resource_record_value"]]

  zone_id = data.aws_route53_zone.default[0].zone_id
}

data "aws_route53_zone" "default" {
  count = local.create_dns_validation_records ? 1 : 0

  name         = local.dns_zone_domain_name
  private_zone = false
}
