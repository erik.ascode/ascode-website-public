# tls-certificate

Terraform module for requesting a ACM TLS certificate with route53 DNS validation

## Usage

The first time the certificate is created DNS validation must be skipped.
The validation resources depend on the amount of domain names to be validates, therefore the count on the validation resources is "computed".
Disabling the validation to let terraform create the certificate, and then enabling it again allows terraform to count how many validation resources it needs.
To disable validation ask terraform to create a plan that targets the certificate alone add `-target module.environment_tls.aws_acm_certificate.default` to your terraform plan command.
Then run terraform plan-apply again to configure validation.
Every time the certificate changes this two-step approach is needed.  

```hcl-terraform
module "tls_cert" {
  source = "git://gitlab.com/ascloudnl/terraform-modules/aws/tls-certificate.git?ref=v1.0.0"
 
  certificate_name = "my-tls-certificate"
  domain_name      = "example.com"
  subject_alternative_names = [
    "foo.example.com",
    "bar.example.com",
  ]

  total_name_count = 3
 
  tags = {
    Name    = "my-tls-certificate"
    project = "my-project"
  }
}
```
