output "name" {
  value = compact(
    concat(
      aws_route53_record.record.*.name,
      aws_route53_record.alias.*.name,
    ),
  )[0]
}

output "fqdn" {
  value = compact(
    concat(
      aws_route53_record.record.*.fqdn,
      aws_route53_record.alias.*.fqdn,
    ),
  )[0]
}
