resource "aws_route53_record" "record" {
  count   = length(var.records) > 0 ? 1 : 0
  zone_id = var.zone_id
  name    = var.name
  type    = var.type
  ttl     = var.ttl
  records = var.records
}

resource "aws_route53_record" "alias" {
  count   = var.alias_name != "" ? 1 : 0
  zone_id = var.zone_id
  name    = var.name
  type    = var.type

  alias {
    name                   = var.alias_name
    zone_id                = var.alias_zone_id
    evaluate_target_health = var.alias_evaluate_target_health
  }
}

