variable "zone_id" {
}

variable "name" {
}

variable "type" {
  default = "A"
}

variable "ttl" {
  default = "300"
}

variable "records" {
  type    = list(string)
  default = []
}

variable "alias_name" {
  default = ""
}

variable "alias_zone_id" {
  default = ""
}

variable "alias_evaluate_target_health" {
  default = true
}
