# Create the OAI 
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "OAI used for S3 web site"
}

# Create the Cloudfront distribution
resource "aws_cloudfront_distribution" "default" {
  origin {
    domain_name = module.root_bucket.this_s3_bucket_bucket_regional_domain_name
    origin_id   = local.s3_origin_id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  origin {
    domain_name = replace(aws_api_gateway_deployment.default.invoke_url, "/^https?://([^/]*).*/", "$1")
    origin_id   = "apigw"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = module.cloudfront_logging_bucket.this_s3_bucket_bucket_regional_domain_name
    prefix          = var.environment
  }

  aliases = [var.root_domain, "www.${var.root_domain}"]

  default_cache_behavior {
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400

    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = aws_lambda_function.redirect_to_apex.qualified_arn
      include_body = false
    }

    lambda_function_association {
      event_type   = "viewer-response"
      lambda_arn   = aws_lambda_function.security_headers.qualified_arn
      include_body = false
    }

    lambda_function_association {
      event_type   = "origin-request"
      lambda_arn   = aws_lambda_function.redirect_to_index.qualified_arn
      include_body = false
    }
  }

  ordered_cache_behavior {
    path_pattern     = "/default/*"
    allowed_methods  = ["HEAD", "DELETE", "POST", "GET", "OPTIONS", "PUT", "PATCH"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = "apigw"

    viewer_protocol_policy = "redirect-to-https"
    default_ttl            = 0
    min_ttl                = 0
    max_ttl                = 0

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
    }
  }

  price_class = var.cf_price_class

  # Here you can implement countries that you want to blacklist - for example France. Use the ISO 3166-1-alpha-2 codes (https://www.iso.org/obp/ui/#search)
  restrictions {
    geo_restriction {
      restriction_type = "blacklist"
      locations        = ["FR"]
    }

    tags = local.tags

    viewer_certificate {
      acm_certificate_arn      = module.environment_tls.arn
      ssl_support_method       = "sni-only"
      minimum_protocol_version = "TLSv1.2_2018"
    }
  }
}

## S3 for Logging
# Create the KMS key for bucket encryption
resource "aws_kms_key" "cloudfront_logging_key" {
  description = "KMS key used to encrypt s3 buckets that contain Cloudfront logs"
  tags        = local.tags

  enable_key_rotation     = true
  deletion_window_in_days = 7

  policy = data.aws_iam_policy_document.kms_policy.json
}

# Create the S3 bucket
module "cloudfront_logging_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "1.4.0"

  bucket = local.cf_bucket_name
  acl    = "private"

  force_destroy = true

  tags = local.tags

  versioning = {
    enabled = false
  }
  policy        = data.aws_iam_policy_document.logging_bucket_policy.json
  attach_policy = true

}
