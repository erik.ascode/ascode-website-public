// this script adds "index.html" to the request as websites that are redirected to a subdirectory are not recognized when using an S3 backend.
// this will cause the "NoSuchKey" error and must be corrected by using this script.

'use strict';

let defaultDocument = 'index.html';

exports.handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;

    if (request.uri != "/") {
        let paths = request.uri.split('/');
        let lastPath = paths[paths.length - 1];
        let isFile = lastPath.split('.').length > 1;

        if (!isFile) {
            if (lastPath != "") {
                request.uri += "/";
            }

            request.uri += defaultDocument;
        }

        console.log(request.uri);
    }

    callback(null, request);
};
