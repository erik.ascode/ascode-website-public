var AWS = require('aws-sdk');
var ses = new AWS.SES();

var RECEIVER = 'info@ascode.nl';
var SENDER = 'contactus@ascode.nl';

exports.handler = function (event, context, callback) {
    var requestBody = JSON.stringify(event.name);
    var responseBody = {};
    var response = {};

    if (
        requestBody
    ) {
        var emailTo = RECEIVER;
        var emailFrom = SENDER;
        var subject = 'Website Referral Form: ' + event.name;
    } else {
        responseBody = {
            result: "fail",
            resultCode: 400,
            description:
                "Incorrect Parameters. Please fill in all fields."
        };

        response = {
            statusCode: 400,
            headers: {
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify(responseBody),
            isBase64Encoded: false
        };

        callback(null, response);
    }

    var emailParams = {
        Destination: {
            ToAddresses: [emailTo]
        },
        Message: {
            Body: {
                Text: {
                    Data: 'name: ' + event.name + '\nphone: ' + event.phone + '\nemail: ' + event.email + '\ndesc: ' + event.desc,
                    Charset: 'UTF-8'
                }
            },
            Subject: {
                Data: subject
            }
        },
        Source: emailFrom
    };

    var email = ses.sendEmail(emailParams, function (err, data) {
        var resultCode = 200;
        if (err) {
            var responseBody = {
                result: "FAIL",
                resultCode: 500,
                description: "Error sending email: " + err
            };
            resultCode = 500;
        } else {
            var responseBody = {
                result: "OK",
                resultCode: 200,
                description: "Email sent successfully"
            };
        }

        response = {
            statusCode: resultCode,
            headers: {
                "Access-Control-Allow-Origin": "*"
            },
            body: JSON.stringify(responseBody),
            isBase64Encoded: false
        };
        callback(null, response);
    });
};