"use strict";
exports.handler = (event, context, callback) => {
  const response = event.Records[0].cf.response;
  const headers = response.headers;

  headers["strict-transport-security"] = [
    {
      key: "Strict-Transport-Security",
      value: "max-age=31536000; includeSubdomains; preload"
    }
  ];

  headers["content-security-policy"] = [
    {
      key: "Content-Security-Policy",
      value:
        "default-src 'self'; font-src 'self' https://fonts.gstatic.com; img-src 'self' data: https://www.google-analytics.com https://maps.gstatic.com https://maps.googleapis.com; script-src 'self' https://www.google-analytics.com/analytics.js https://www.googletagmanager.com/gtag/js https://www.google.com/recaptcha/api.js https://maps.googleapis.com https://maps.gstatic.com https://www.gstatic.com; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com/; object-src 'none'"
    }
  ];

  headers["x-content-type-options"] = [
    {
      key: "X-Content-Type-Options",
      value: "nosniff"
    }
  ];

  headers["x-frame-options"] = [
    {
      key: "X-Frame-Options",
      value: "DENY"
    }
  ];

  headers["x-xss-protection"] = [
    {
      key: "X-XSS-Protection",
      value: "1; mode=block"
    }
  ];

  headers["referrer-policy"] = [
    {
      key: "Referrer-Policy",
      value: "same-origin"
    }
  ];

  callback(null, response);
};
