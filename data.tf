# Get the current account id
data "aws_caller_identity" "current" {}

# KMS key policy to allow Cloudfront to write to S3
data "aws_iam_policy_document" "kms_policy" {
  statement {
    sid = "Allow CloudFront Flow Logs to use the key"
    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = [
      "kms:GenerateDataKey*",
    ]

    resources = ["arn:aws:kms:${var.aws_region}:${local.aws_account_id}:key/*"]
  }

  statement {
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }

    actions = [
      "kms:*",
    ]

    resources = ["*"]
  }
}

# S3 bucket policy 
data "aws_iam_policy_document" "logging_bucket_policy" {
  statement {
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }

    actions = [
      "s3:GetBucketACL",
      "s3:PutBucketACL"
    ]

    resources = [
      "arn:aws:s3:::${local.cf_bucket_name}"
    ]
  }
}

# Assumerole policy for Lambda and Lambda@Edge
data "aws_iam_policy_document" "assume_role_lambda" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "lambda.amazonaws.com",
        "edgelambda.amazonaws.com",
      ]
    }
  }
}

# Assumerole policy for Lambda and API Gateway
data "aws_iam_policy_document" "assume_role_just_lambda" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "lambda.amazonaws.com",
        "apigateway.amazonaws.com"
      ]
    }
  }
}

# Allow Lambda to send mail and API gateway to invoke the lambda function
data "aws_iam_policy_document" "ses_sendmail_lambda" {
  statement {
    effect = "Allow"

    actions = [
      "ses:SendEmail",
    ]

    resources = ["*"]

  }

  statement {
    sid    = "AllowExecutionFromAPIGateway"
    effect = "Allow"

    actions = [
      "lambda:InvokeFunction",
    ]

    condition {
      test     = "ArnLike"
      variable = "AWS:SourceArn"
      values   = ["${aws_api_gateway_rest_api.contact_us_api.execution_arn}/*/*/*"]
    }

    resources = [aws_lambda_function.ses_send_mail.arn]
  }

  statement {
    sid    = "WriteToCloudWatch"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["*"]
  }
}

data "aws_iam_policy_document" "cloudwatch_lambda" {
  statement {
    sid    = "WriteToCloudWatch"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["*"]
  }
}
