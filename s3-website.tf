# S3 vucket policy to allow the Cloudfront OAI access to the bucket
data "aws_iam_policy_document" "root_bucket_policy" {
  statement {
    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }

    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${var.root_domain}",
    ]
  }

  statement {
    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::${var.root_domain}/*",
    ]
  }
}

# create the S3 bucket that contains the files for the website
module "root_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "1.4.0"

  create_bucket = var.environment == "prd" ? true : false
  bucket        = var.root_domain
  acl           = "private"

  force_destroy = true

  tags = local.tags

  versioning = {
    enabled = true
  }
  policy        = data.aws_iam_policy_document.root_bucket_policy.json
  attach_policy = true

}
