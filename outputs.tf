
output "terraform_account_profile" {
  value = "${var.customer_tla}-${var.environment}"
}

# tls certificates
output "environment_tls_cert_arn" {
  value = module.environment_tls.arn
}

# OAI
output "origin_access_identity_arn" {
  value = aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn
}

# API
output "api_invoke_url" {
  value = aws_api_gateway_deployment.ses_sendmail.invoke_url
}

output "api_key_value" {
  value = aws_api_gateway_api_key.default.value
}

output "api_key_id" {
  value = aws_api_gateway_api_key.default.id
}

output "api_key_arn" {
  value = aws_api_gateway_api_key.default.arn
}
