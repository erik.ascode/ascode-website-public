# determine local zone
data "aws_route53_zone" "environment" {
  name         = local.environment_domain_name
  private_zone = false
}

## Zone
module "route53_root_zone" {
  source = "./modules/route53/route53-zone"
  domain = var.root_domain
  tags   = local.tags
}

## Record
### www
module "route53_alias_www_record" {
  source                       = "./modules/route53/route53-record"
  name                         = "www.${var.root_domain}"
  type                         = "A"
  zone_id                      = module.route53_root_zone.zone_id
  alias_zone_id                = aws_cloudfront_distribution.default.hosted_zone_id
  alias_name                   = aws_cloudfront_distribution.default.domain_name
  alias_evaluate_target_health = false
}

### apex
module "route53_alias_naked_record" {
  source                       = "./modules/route53/route53-record"
  name                         = var.root_domain
  type                         = "A"
  zone_id                      = module.route53_root_zone.zone_id
  alias_zone_id                = aws_cloudfront_distribution.default.hosted_zone_id
  alias_name                   = aws_cloudfront_distribution.default.domain_name
  alias_evaluate_target_health = false
}
