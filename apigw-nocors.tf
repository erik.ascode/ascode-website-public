# assumerole policy for API Gateway
data "aws_iam_policy_document" "assume_role_apigateway" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "apigateway.amazonaws.com"
      ]
    }
  }
}

# Policy for writing to CloudWatch
data "aws_iam_policy_document" "cloudwatch" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents",
    ]

    resources = ["*"]

  }
}

# Create API Key
resource "aws_api_gateway_api_key" "default" {
  name    = var.api_key_name
  enabled = true
  tags    = local.tags
}

# Create an API Gateway usage plan to enable throttling
resource "aws_api_gateway_usage_plan" "default" {
  name = "${aws_api_gateway_rest_api.default.name}-usage-plan"
  api_stages {
    api_id = aws_api_gateway_rest_api.default.id
    stage  = aws_api_gateway_deployment.default.stage_name
  }

  quota_settings {
    limit  = var.usage_quota_limit
    offset = var.usage_quota_offset
    period = var.usage_quota_period
  }

  throttle_settings {
    burst_limit = var.usage_throttle_burst_limit
    rate_limit  = var.usage_throttle_rate_limit
  }
}

# Assign the API key to the usage plan
resource "aws_api_gateway_usage_plan_key" "default" {
  key_id        = aws_api_gateway_api_key.default.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.default.id
}

# Create the API
resource "aws_api_gateway_rest_api" "default" {
  name        = "default"
  description = "This API sends en email using SES as part of a website contact form"

  endpoint_configuration {
    types = ["EDGE"]
  }
}

# create the API method with CloudWatch logging
resource "aws_api_gateway_method_settings" "logging" {
  depends_on = [aws_api_gateway_account.default]

  rest_api_id = aws_api_gateway_rest_api.default.id
  stage_name  = var.environment
  method_path = "*/*"

  settings {
    metrics_enabled        = var.api_gw_metrics_enabled
    logging_level          = var.api_gw_logging_level
    throttling_rate_limit  = var.throttling_rate_limit
    throttling_burst_limit = var.throttling_burst_limit
  }
}

# assign the CloudWatch IAM role to the account 
resource "aws_api_gateway_account" "default" {
  depends_on = [aws_iam_role.cloudwatch]

  cloudwatch_role_arn = aws_iam_role.cloudwatch.arn
}

# Create the CloudWatch IAM role
resource "aws_iam_role" "cloudwatch" {
  name               = "cloudwatch"
  assume_role_policy = data.aws_iam_policy_document.assume_role_apigateway.json
}

# create the IAM Policy for CloudWatch
resource "aws_iam_policy" "cloudwatch" {
  path   = "/"
  policy = data.aws_iam_policy_document.cloudwatch.json
}

# Attach the CloudWatch policy to the CloudWatch role
resource "aws_iam_role_policy_attachment" "cloudwatch_apigateway" {
  role       = aws_iam_role.cloudwatch.name
  policy_arn = aws_iam_policy.cloudwatch.arn
}

# Create am API resource, in this case on /default
resource "aws_api_gateway_resource" "default_resource" {
  rest_api_id = aws_api_gateway_rest_api.default.id
  parent_id   = aws_api_gateway_rest_api.default.root_resource_id
  path_part   = "default"
}

# Create and API method for the /default resource
resource "aws_api_gateway_method" "default_method" {
  rest_api_id      = aws_api_gateway_rest_api.default.id
  resource_id      = aws_api_gateway_resource.default_resource.id
  http_method      = "POST"
  authorization    = "NONE"
  api_key_required = true
}

# Create and API Lambda integration for the /default resource
resource "aws_api_gateway_integration" "lambda_integration" {
  rest_api_id             = aws_api_gateway_rest_api.default.id
  resource_id             = aws_api_gateway_resource.default_resource.id
  http_method             = aws_api_gateway_method.default_method.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  content_handling        = "CONVERT_TO_TEXT"
  uri                     = aws_lambda_function.ses_send_mail.invoke_arn
}

# Create and API integration response for the /default resource
resource "aws_api_gateway_integration_response" "default_response" {
  depends_on = [aws_api_gateway_integration.lambda_integration, aws_api_gateway_method_response.lambda_response]

  rest_api_id = aws_api_gateway_rest_api.default.id
  resource_id = aws_api_gateway_resource.default_resource.id
  http_method = aws_api_gateway_method.default_method.http_method
  status_code = 200

  response_parameters = local.integration_response_parameters
}

# Create and API method response for the /default resource
resource "aws_api_gateway_method_response" "lambda_response" {
  depends_on = [aws_api_gateway_method.default_method]

  rest_api_id = aws_api_gateway_rest_api.default.id
  resource_id = aws_api_gateway_resource.default_resource.id
  http_method = aws_api_gateway_method.default_method.http_method
  status_code = 200

  response_parameters = local.method_response_parameters
}

# Allow the API Gateway to execute the Lambda function
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ses_send_mail.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.default.execution_arn}/*/*/*"
}

# Create an API Gatway deployment, redeploy when 1 of the configuration items for the resource changes
resource "aws_api_gateway_deployment" "default" {
  depends_on = [aws_api_gateway_integration.lambda_integration]

  rest_api_id = aws_api_gateway_rest_api.default.id
  stage_name  = var.environment

  lifecycle {
    create_before_destroy = true
  }

  # add variables to make sure the api is redeployed when 1 of these resources changes
  variables = {
    trigger_hash = sha1(join(",", [
      jsonencode(aws_api_gateway_method_response.lambda_response),
      jsonencode(aws_api_gateway_integration_response.default_response),
      jsonencode(aws_api_gateway_resource.default_resource),
      jsonencode(aws_api_gateway_method.default_method),
      jsonencode(aws_api_gateway_integration.lambda_integration),
    ]))
  }
}
