variable "account_id" {
  type = map(string)
  default = {
    root = ""
    dev  = ""
    tst  = ""
    acc  = ""
    prd  = ""
    svc  = ""
  }
}

variable "environment" {
  type        = string
  default     = ""
  description = "The environment to deploy the resources in"
}

variable "aws_region" {
  type    = string
  default = "eu-west-1"
}

variable "customer_tla" {
  type        = string
  description = "The three-letter-acronym for the cust0mer"
  default     = ""
}

variable "customer_domain" {
  type        = string
  description = "the customer domain name, like customer.example.com"
  default     = ""
}

variable "root_domain" {
  type        = string
  description = "The root domain name, like example.com"
  default     = ""
}

variable "api_key_name" {
  type        = string
  description = "The name to use for the api key"
  default     = ""
}

variable "usage_quota_limit" {
  type        = number
  description = "The maximum number of requests that can be made in a given time period"
  default     = 50
}

variable "usage_quota_offset" {
  type        = number
  description = "The number of requests subtracted from the given limit in the initial time period"
  default     = 0
}

variable "usage_quota_period" {
  type        = string
  description = "The time period in which the limit applies"
  default     = "DAY"
}

variable "usage_throttle_burst_limit" {
  type        = number
  description = "The API request burst limit, the maximum rate limit over a time ranging from one to a few seconds, depending upon whether the underlying token bucket is at its full capacity"
  default     = 5
}

variable "usage_throttle_rate_limit" {
  type        = number
  description = "The API request steady-state rate limit"
  default     = 1
}

variable "api_gw_metrics_enabled" {
  type        = bool
  description = "Enable API Gateway metrics and logging"
  default     = true
}

variable "api_gw_logging_level" {
  type        = string
  description = "The logging level for the API Gateway"
  default     = "ERROR"
}

variable "throttling_rate_limit" {
  type        = number
  description = "Specifies the throttling rate limit"
  default     = 100
}

variable "throttling_burst_limit" {
  type        = number
  description = "Specifies the throttling burst limit"
  default     = 50
}

variable "cf_price_class" {
  type        = string
  description = "The Cloudfront price class"
  default     = "PriceClass_100"
}

variable "allow_headers" {
  type        = list(string)
  description = "Allow headers"

  default = [
    "Authorization",
    "Content-Type",
    "X-Amz-Date",
    "X-Amz-Security-Token",
    "X-Api-Key",
  ]
}

variable "allow_methods" {
  type        = list(string)
  description = "Allow methods"

  default = [
    "OPTIONS",
    "HEAD",
    "GET",
    "POST",
    "PUT",
    "PATCH",
    "DELETE",
  ]
}

variable "allow_origin" {
  type        = string
  description = "Allow origin"
  default     = "*"
}

variable "allow_max_age" {
  type        = string
  description = "Allow response caching time"
  default     = "7200"
}

variable "allow_credentials" {
  type        = bool
  description = "Allow credentials"
  default     = false
}

variable "cf_bucket_name" {
  type        = string
  description = "The name for the cloudfront logging bucket"
  default     = "cf-logs"
}
