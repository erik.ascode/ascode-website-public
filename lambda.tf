# Lambda expects a zip file, so here we create those
data "archive_file" "lambda_apex_redirect" {
  type        = "zip"
  source_file = "lambda/redirect-apex.js"
  output_path = "lambda/redirect-apex.zip"
}

data "archive_file" "lambda_send_email_ses" {
  type        = "zip"
  source_file = "lambda/send-email-ses.js"
  output_path = "lambda/send-email-ses.zip"
}

data "archive_file" "lambda_index_redirect" {
  type        = "zip"
  source_file = "lambda/add-index.js"
  output_path = "lambda/add-index.zip"
}

data "archive_file" "lambda_security_headers" {
  type        = "zip"
  source_file = "lambda/security-headers.js"
  output_path = "lambda/security-headers.zip"
}

# create the IAM role for the apex redirect function
resource "aws_iam_role" "apex_redirect_lambda" {
  name               = "apex-redirect-lambda"
  assume_role_policy = data.aws_iam_policy_document.assume_role_lambda.json
  tags               = local.tags
}

# create the IAM role for the sendmail function
resource "aws_iam_role" "ses_sendmail_lambda" {
  name               = "SES-sendmail-lambda"
  assume_role_policy = data.aws_iam_policy_document.assume_role_just_lambda.json
  tags               = local.tags
}

# create the IAM role for the security-headers function
resource "aws_iam_role" "security_headers_lambda" {
  name               = "security-headers-lambda"
  assume_role_policy = data.aws_iam_policy_document.assume_role_lambda.json
  tags               = local.tags
}

# create the policy for the sendmail role
resource "aws_iam_policy" "ses_sendmail" {
  path   = "/"
  policy = data.aws_iam_policy_document.ses_sendmail_lambda.json
}

# create the policy for writing to cloudwatch
resource "aws_iam_policy" "cloudwatch_lambda" {
  path   = "/"
  policy = data.aws_iam_policy_document.cloudwatch_lambda.json
}

# attach the sendmail policy to the sendmail role
resource "aws_iam_role_policy_attachment" "default" {
  role       = aws_iam_role.ses_sendmail_lambda.name
  policy_arn = aws_iam_policy.ses_sendmail.arn
}

# attach the cloudwatch policy to the security-headers role
resource "aws_iam_role_policy_attachment" "cloudwatch_lambda" {
  role       = aws_iam_role.security_headers_lambda.name
  policy_arn = aws_iam_policy.cloudwatch_lambda.arn
}

# create the apex redirect Lambda
resource "aws_lambda_function" "redirect_to_apex" {
  description   = "redirect www to apex"
  function_name = "www_to_apex_redirect"

  filename         = data.archive_file.lambda_apex_redirect.output_path
  source_code_hash = data.archive_file.lambda_apex_redirect.output_base64sha256
  timeout          = 5

  role    = aws_iam_role.apex_redirect_lambda.arn
  handler = "redirect-apex.handler"
  runtime = "nodejs10.x"

  publish = true

  provider = aws.useast
}

# create the sendmail Lambda
resource "aws_lambda_function" "ses_send_mail" {
  description   = "send email using SES"
  function_name = "ses_send_mail"

  filename         = data.archive_file.lambda_send_email_ses.output_path
  source_code_hash = data.archive_file.lambda_send_email_ses.output_base64sha256
  timeout          = 5

  role    = aws_iam_role.ses_sendmail_lambda.arn
  handler = "send-email-ses.handler"
  runtime = "nodejs10.x"
}

# create the subfolder redirect IAM role
resource "aws_iam_role" "subfolder_redirect_lambda" {
  name               = "subfolder-redirect-lambda"
  assume_role_policy = data.aws_iam_policy_document.assume_role_lambda.json
}

# create the subfolder redirect Lambda
resource "aws_lambda_function" "redirect_to_index" {
  description   = "redirect subfolder to index.html"
  function_name = "subfolder_to_index_redirect"

  filename         = data.archive_file.lambda_index_redirect.output_path
  source_code_hash = data.archive_file.lambda_index_redirect.output_base64sha256
  timeout          = 5

  role    = aws_iam_role.subfolder_redirect_lambda.arn
  handler = "add-index.handler"
  runtime = "nodejs10.x"

  publish = true

  provider = aws.useast
}

# create the security headers Lambda
resource "aws_lambda_function" "security_headers" {
  description   = "apply security headers"
  function_name = "security_headers"

  filename         = data.archive_file.lambda_security_headers.output_path
  source_code_hash = data.archive_file.lambda_security_headers.output_base64sha256
  timeout          = 5

  role    = aws_iam_role.security_headers_lambda.arn
  handler = "security-headers.handler"
  runtime = "nodejs10.x"

  publish = true

  provider = aws.useast
}
