# default AWS provider
provider "aws" {
  region  = var.aws_region
  profile = "default"

  skip_metadata_api_check = true
}

# AWS provider in us-east-1 region for Cloudfront Lamda and ACM asssociations
provider "aws" {
  alias   = "useast"
  region  = "us-east-1"
  profile = "default"

  skip_metadata_api_check = true
}
